package edu.udayton.pantry.pantryio;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class RecipeActivity extends AppCompatActivity {
    private ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        mListView = (ListView) findViewById(R.id.recipe_list_view);
// 1
        final ArrayList<Recipe> recipeList = new ArrayList<Recipe>();
        SQLiteDatabase pantrydb = openOrCreateDatabase("pantrydb",MODE_PRIVATE,null);

        Cursor c = pantrydb.rawQuery("SELECT ID,name FROM recipe", null);
        if (c.moveToFirst()){
            do {
// Passing values
                String column1 = c.getString(0);
                String column2 = c.getString(1);

                int id = Integer.valueOf(column1);
                Recipe currentRecipe = new Recipe(id,column2);
                recipeList.add(currentRecipe);
// Do something Here with values

            } while(c.moveToNext());
        }
        c.close();

//
        String[] listRecipes = new String[recipeList.size()];
//
        for(int i = 0; i < recipeList.size(); i++){
            Recipe recipe = recipeList.get(i);
            listRecipes[i] = recipe.name;
            System.out.println(recipe.name);
        }
//
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listRecipes);
        mListView.setAdapter(adapter);
    }
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_recipe, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        //respond to menu item selection
        switch (item.getItemId()) {
            case R.id.action_items:
                startActivity(new Intent(this, PantryItemActivity.class));
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
