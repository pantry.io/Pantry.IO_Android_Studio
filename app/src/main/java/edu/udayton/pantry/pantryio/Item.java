package edu.udayton.pantry.pantryio;

/**
 * Created by JackLuzik on 12/3/17.
 */

public class Item {
    int id;
    String name;

    public Item(int id,String name) {
        this.id = id;
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

