package edu.udayton.pantry.pantryio;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class PantryItemActivity extends AppCompatActivity {
    private ListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantry_item);
        mListView = (ListView) findViewById(R.id.item_list_view);
// 1
        final ArrayList<Item> itemList = new ArrayList<Item>();
        SQLiteDatabase pantrydb = openOrCreateDatabase("pantrydb",MODE_PRIVATE,null);

        Cursor c = pantrydb.rawQuery("SELECT ID,name FROM item", null);
        if (c.moveToFirst()){
            do {
// Passing values
                String column1 = c.getString(0);
                String column2 = c.getString(1);

                int id = Integer.valueOf(column1);
                Item currentItem = new Item(id,column2);
                itemList.add(currentItem);
// Do something Here with values

            } while(c.moveToNext());
        }
        c.close();

//
        String[] listItems = new String[itemList.size()];
//
        for(int i = 0; i < itemList.size(); i++){
            Item item = itemList.get(i);
            listItems[i] = item.name;
            System.out.println(item.name);
        }
//
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems);
        mListView.setAdapter(adapter);
    }
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_pantry_item, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {

        //respond to menu item selection
        switch (item.getItemId()) {
            case R.id.action_recipes:
                startActivity(new Intent(this, RecipeActivity.class));
                return true;
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
