package edu.udayton.pantry.pantryio;

import android.database.sqlite.SQLiteDatabase;

import static android.database.sqlite.SQLiteDatabase.openOrCreateDatabase;

/**
 * Created by ryanberry on 12/4/17.
 */

public class DatabaseHandler extends LoginActivity{
    SQLiteDatabase pantrydb = openOrCreateDatabase("pantrydb",0,null);

    public SQLiteDatabase getDatabase()
    {
        return pantrydb;
    }

    public void onCreate()
    {

    }

    public void onUpgrade()
    {

    }
}
